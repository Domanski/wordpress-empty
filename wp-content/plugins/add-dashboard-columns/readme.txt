=== Add Dashboard Columns ===
Contributors: 23r9i0
Donate link:
Tags: dashboard, widget, dashboard columns, columns
Requires at least: 3.8
Tested up to: 3.8.1
Stable tag: 0.2.1
License: GPL/MIT

Enable Dashboard Columns in WordPress after version 3.8

== Description ==

Enable Dashboard Columns in WordPress after version 3.8

== Installation ==

* Upload the 'add-dashboard-columns' folder to the '/wp-content/plugins/' directory.
* Activate the plugin through the 'Plugins' menu in WordPress.

== Changelog ==
= 0.2.1 =
* fix Readme.txt
= 0.2 =
* Set Default Columns in three
* Only load CSS in index
* fix constant developer
= 0.1 =
* First Version
