<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

<meta name="msapplication-TileColor" content="#ffc40d">
<meta name="msapplication-TileImage" content="/mstile-144x144.png">
<meta name="theme-color" content="#ffffff">
<meta name="application-name" content="<?php bloginfo('name'); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="<?php bloginfo('description'); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="p-all">
    <header class="p-head">
        <div class="p-center">
            <a class="p-logo" href="<?php echo home_url(); ?>">
                <img src="<?php echo get_template_directory_uri(); ?>/images/.....png" class="p-logo-img" alt="" />
            </a>

            <nav class="p-menu" role="navigation">
	            <?php insert_menu_ul('glowne', 'p-menu-list'); ?>
            </nav>

	        <?php get_search_form( true ); ?>
        </div>
    </header>