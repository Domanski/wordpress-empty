<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
    <label for="s">Szukaj:</label>
    <input type="text" name="s" id="s" value="<?php echo get_search_query(); ?>" />
    <button type="submit">Szukaj</button>
</form>