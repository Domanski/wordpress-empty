/**
 * Created by kartofelek on 2016-03-11 00:36.
 */

var scripts
.
min.js = (function () {
	"use strict";

	var _privateMethod = function () {

	};

	var _init = function () {
		$('html').removeClass('no-js');
		_privateMethod();
	};

	return {
		init: _init
	}
})();

$(function ($) {
	API.init();
});