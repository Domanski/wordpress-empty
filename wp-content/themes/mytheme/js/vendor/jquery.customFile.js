/*----------------
form style v07 
$('.form').styleFormElements();
$('input').trigger('disable');
$('input').trigger('enable');
----------------*/
$.fn.styleFile = function(options) {
    options = $.extend({
        btnClass : ''
    }, options);

    return this.each(function() {
        var styleFile=function($element){
            var classList = '';
            if ($element.attr('class') != undefined) {
                classList = $element.attr('class');
                $element.attr('class', '');
            }
            var labelText = ($element.attr('title')!='' && $element.attr('title')!=undefined)? $element.attr('title'):'Wybierz';
            var disabled = $element.prop('disabled') ? ' disabled' : '';
            var $container = $('<div class="file-cnt" />');
            var $textFile = $('<span class="text">'+$element.val()+'</span>');
            var $inputFileWrapper = $('<div class="input-file '+options.btnClass+'"><span>'+labelText+'</span></div>');

            $container.append($inputFileWrapper).append($textFile);
            $element.replaceWith($container);
            $container.find('.input-file').append($element);
            $element.css({'font-size' : 1000, opacity:0});
            $element.on('change click', function() {
                var $this = $(this);
                $this.parent().parent().find('.text').text($this.val());
            });
            var oldClassList = classList.split(' ');
            $.each(oldClassList, function(k) {
                $container.addClass(oldClassList[k]);
            });
            $container.addClass(disabled);
            $element.addClass('styled');
        };

        var disableStyledElement = function() {
            var $this = $(this);
            var $parent = $this.closest('.file-cnt');
            $this.attr('disabled', 'disabled');
            if ($parent.hasClass('styled')) $parent.addClass('disabled');                
        };

        var enableStyledElement = function() {
            var $this = $(this);
            var $parent = $this.closest('.file-cnt');
            $this.removeAttr('disabled');
            $parent.removeClass('disabled');
        };

        var $this = $(this);
        
        if ($this.is(':file')) styleFile($this);

        $this.on({
            'disable' : disableStyledElement,
            'enable' : enableStyledElement
        })

    });    
}; /* styleFormElements */
