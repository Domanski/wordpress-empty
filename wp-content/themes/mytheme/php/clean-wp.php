<?php
//-----------------------------------------------------------
// clean wordpress
//-----------------------------------------------------------
add_action('init', 'head_cleanup'); // launching operation cleanup



function head_cleanup()
{
	add_action( 'init', 'disable_wp_emojicons' );
	remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
	remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
	remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
	remove_action('wp_head', 'index_rel_link'); // Index link
	remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
	remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
	remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
	remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
	remove_action('wp_head', 'rel_canonical');
	remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
	remove_action('wp_head', 'rsd_link'); // EditURI link
	remove_action('wp_head', 'wlwmanifest_link'); // windows live writer
	remove_action('wp_head', 'index_rel_link'); // index link
	remove_action('wp_head', 'parent_post_rel_link', 10, 0); // previous link
	remove_action('wp_head', 'start_post_rel_link', 10, 0); // start link
	remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0); // links for adjacent posts
	remove_action('wp_head', 'wp_generator');// WP version

	// all actions related to emojis
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

	global $wp_widget_factory;
	remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));

	// all actions related to emojis
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
}


//-----------------------------------------------------------
// remove gutenberg block styles
//-----------------------------------------------------------
add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css', 100 );
function smartwp_remove_wp_block_library_css(){
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
    wp_dequeue_style( 'wc-block-style' ); // Remove WooCommerce block CSS
}


//-----------------------------------------------------------
//change read more
//-----------------------------------------------------------
add_filter('excerpt_more','new_excerpt_more',11);
function new_excerpt_more($more) {
	global $post;
	remove_filter('excerpt_more', 'new_excerpt_more');
	return ' <a class="post-read-more" href="'. get_permalink($post->ID) . '">' . 'Czytaj więcej' . '</a>';
}




//-----------------------------------------------------------
//remove 32px margin top when admin bar is visible
//-----------------------------------------------------------
add_action('get_header', 'remove_admin_login_header');
function remove_admin_login_header() {
	remove_action('wp_head', '_admin_bar_bump_cb');
}



//-----------------------------------------------------------
// Remove invalid rel attribute values in the categorylist
//-----------------------------------------------------------
add_filter('the_category', 'remove_category_rel_from_category_list');
function remove_category_rel_from_category_list($thelist)
{
	return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}


//-----------------------------------------------------------
// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
//-----------------------------------------------------------
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10);
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10);
function remove_thumbnail_dimensions($html)
{
	$html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
	return $html;
}


//-----------------------------------------------------------
// Remove 'text/css' from our enqueued stylesheet
//-----------------------------------------------------------
add_filter('style_loader_tag', 'style_remove');
function style_remove($tag)
{
	return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}


//-----------------------------------------------------------
// remove WP version from RSS
//-----------------------------------------------------------
add_filter('the_generator', 'rss_version');
function rss_version()
{
	return '';
}


//-----------------------------------------------------------
// This removes the annoying […] to a Read More link
//-----------------------------------------------------------
add_filter('excerpt_more', 'excerpt_more');
function excerpt_more($more)
{
	global $post;
	// edit here if you like
	return '...  <a class="excerpt-read-more" href="' . get_permalink($post->ID) . '">Read more &raquo</a>';
}


//-----------------------------------------------------------
// remove injected CSS for recent comments widget
//-----------------------------------------------------------
add_filter('wp_head', 'remove_wp_widget_recent_comments_style', 1);
function remove_wp_widget_recent_comments_style()
{
	if (has_filter('wp_head', 'wp_widget_recent_comments_style')) {
		remove_filter('wp_head', 'wp_widget_recent_comments_style');
	}
}


//-----------------------------------------------------------
// remove injected CSS from recent comments widget
//-----------------------------------------------------------
add_action('wp_head', 'remove_recent_comments_style', 1);
function remove_recent_comments_style()
{
	global $wp_widget_factory;
	if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
		remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
	}
}

//-----------------------------------------------------------
// remove injected CSS from gallery
//-----------------------------------------------------------
add_filter('gallery_style', 'gallery_style');
function gallery_style($css)
{
	return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
}
