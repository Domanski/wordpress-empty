<?php
//-----------------------------------------------------------
//wstawia menu w wybrane miejsce
//-----------------------------------------------------------
function insert_menu_ul($menuName = "header-menu", $ulClass = "")
{
	wp_nav_menu(
		array(
            'menu' =>  $menuName,
			'container' => false,
			'container_class' => 'menu-{menu slug}-container',
            'menu_class' => $ulClass,
            'items_wrap' => '<ul class="%2$s">%3$s</ul>',
			'echo' => true,
			'depth' => 0,
			'walker' => '',
		)
	);
}
