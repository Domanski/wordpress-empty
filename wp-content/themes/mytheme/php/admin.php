<?php
//-----------------------------------------------------------
//return current post type
//-----------------------------------------------------------
function get_current_post_type()
{
	global $post, $typenow, $current_screen;
	if ($post && $post->post_type) {
		return $post->post_type;
	} elseif ($typenow) {
		return $typenow;
	} elseif ($current_screen && $current_screen->post_type) {
		return $current_screen->post_type;
	} elseif (isset($_REQUEST['post_type'])) {
		return sanitize_key($_REQUEST['post_type']);
	} elseif (isset($_GET['post'])) {
		$thispost = get_post($_GET['post']);
		return $thispost->post_type;
	} else {
		return null;
	}
}



//-----------------------------------------------------------
//hide gutenberg
//-----------------------------------------------------------
add_action('admin_init', 'hide_editor');
function hide_editor()
{
	$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'];
	if (!isset($post_id)) return;

	$title = get_the_title($post_id);
	$post_type = get_post_type($post_id);

	$titles = [
		"Strona główna",
		"Kontakt",
		"Galeria",
		"Usługi",
		"O nas",
		"Blog"
	];
	$post_types = [
		'gallery'
	];

	if (in_array($title, $titles) || in_array($post_type, $post_types)) {
		remove_post_type_support('page', 'editor');
	}
}



//-----------------------------------------------------------
// remove cap from roles
//-----------------------------------------------------------
function list_editable_roles()
{
	global $wp_roles;

	$all_roles = $wp_roles->roles;
	$editable_roles = apply_filters('editable_roles', $all_roles);

	return $editable_roles;
}


//-----------------------------------------------------------
//remove pages from admin menu
//-----------------------------------------------------------
// add_action('admin_menu', 'remove_admin_bar_links');
function remove_admin_bar_links()
{
	remove_menu_page('edit-comments.php');
	remove_menu_page('edit.php'); //remove posts from admin
	if (!current_user_can('manage_options')) {
		remove_menu_page( 'tools.php' );
	}
}


// //-----------------------------------------------------------
// // Modify TinyMCE editor buttons for styles
// //-----------------------------------------------------------
// add_filter( 'mce_buttons_2', 'fb_mce_editor_buttons' );
function fb_mce_editor_buttons( $buttons ) {
	$value = array_search( 'formatselect', $buttons );
	if ( FALSE !== $value ) {
		foreach ( $buttons as $key => $value ) {
			if ( 'formatselect' === $value )
				unset( $buttons[$key] );
		}
	}

	array_unshift( $buttons, 'styleselect' );
	return $buttons;
}


//add_filter( 'tiny_mce_before_init', 'fb_mce_before_init' );
function fb_mce_before_init( $settings ) {
	$style_formats = array(
		array(
			'title' => 'Headers',
			'items' => array(
				array(
					'title' => 'Header 2',
					'format' => 'h2',
					'icon' => 'bold',
					'wrapper' => true
				),
				array(
					'title' => 'Header 3',
					'format' => 'h3',
					'icon' => 'bold',
					'wrapper' => true
				)
			)
		),
		array(
			'title' => 'Inline',
			'items' => array(
				array(
					'title' => 'Biały tekst',
					'inline' => 'strong',
					'icon' => 'bold',
					'classes' => 'strong',
					'wrapper' => false
				)
			)
		)
	);
	$settings['style_formats'] = json_encode( $style_formats );
	return $settings;
}


//-----------------------------------------------------------
//remove admin bar elements
//-----------------------------------------------------------
//add_action('wp_before_admin_bar_render', 'remove_admin_bar_elements'); //remove admin bar elements
function remove_admin_bar_elements()
{
	global $wp_admin_bar;

	$wp_admin_bar->remove_menu('wp-logo'); // Remove the WordPress logo
	$wp_admin_bar->remove_menu('about'); // Remove the about WordPress link
	$wp_admin_bar->remove_menu('wporg'); // Remove the WordPress.org link
	$wp_admin_bar->remove_menu('documentation'); // Remove the WordPress documentation link
	$wp_admin_bar->remove_menu('support-forums'); // Remove the support forums link
	$wp_admin_bar->remove_menu('feedback'); // Remove the feedback link
	$wp_admin_bar->remove_menu('view-site'); // Remove the view site link
	$wp_admin_bar->remove_menu('comments'); // Remove the comments link
	$wp_admin_bar->remove_menu('w3tc'); // If you use w3 total cache remove the performance link
}


//-----------------------------------------------------------
// disable edit link button for Pages (after permalink on edit page)
//-----------------------------------------------------------
function replacePermalinkInEdit($in)
{
	$out = preg_replace('/<span id="edit-slug-buttons">.*?<\/span>/i', '', $in);
	return $out;
}


//-----------------------------------------------------------
//check user type and disable permalink edit
//-----------------------------------------------------------
add_filter('current_screen', 'checkScreenForRemoveEditButton'); //check current edit page and disable edit link button
function checkScreenForRemoveEditButton($screen)
{
	if ($screen->post_type == 'page') {
		if (current_user_can('editor')) {
			add_filter('get_sample_permalink_html', 'replacePermalink', '', 4);
		}
	}
}


//-----------------------------------------------------------
//remove widgets from dashboard
//-----------------------------------------------------------
add_action('admin_init', 'example_remove_dashboard_widget');
function example_remove_dashboard_widget()
{

	if (is_admin()) {
		remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal'); //incoming links
		remove_meta_box('dashboard_plugins', 'dashboard', 'normal'); //plugins
		remove_meta_box('dashboard_primary', 'dashboard', 'normal'); //wordpress blog
		remove_meta_box('dashboard_secondary', 'dashboard', 'normal'); // other wordpress news
		//remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' ); //szybki szkic
		remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side'); //szkice
		remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal'); //komentarze
		remove_meta_box('dashboard_right_now', 'dashboard', 'normal'); //w skrocie
		remove_meta_box('dashboard_activity', 'dashboard', 'normal'); //aktywność
		remove_action('welcome_panel', 'wp_welcome_panel'); //witaj wordpress
	}
}


//-----------------------------------------------------------
// Disable delete if loged user is not autor and admin
//-----------------------------------------------------------
//add_action('trash_post', 'restrict_post_deletion', 10, 1);
//add_action('delete_post', 'restrict_post_deletion', 10, 1);
function restrict_post_deletion($post_ID)
{
	global $post;
	global $current_user;
	get_currentuserinfo();

	if (is_admin() && is_user_logged_in() && $current_user->ID != $post->post_author) {
		echo "Niestety nie możesz usunąć tej strony.";
	}
}


//-----------------------------------------------------------
// Hide edit, delete on list if loged user is not autor
//-----------------------------------------------------------
add_filter('page_row_actions','hideDeletedLinks',10,2);
function hideDeletedLinks($actions, $post)
{
	global $post;
	global $current_user;
	get_currentuserinfo();

	if (is_admin() && is_user_logged_in() && $current_user->ID != $post->post_author) {
			unset( $actions['inline hide-if-no-js'] );
			unset( $actions['trash'] );
			//unset( $actions['view'] );
			unset( $actions['edit'] );
	}
	return $actions;
}