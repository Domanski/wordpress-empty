<?php
//-----------------------------------------------------------
//register custom post
//-----------------------------------------------------------
add_action('init', 'my_custom_post_product');
function my_custom_post_product()
{
	register_post_type('news', array(
		'labels' => array(
				'name' => _x('news', 'post type general name'),
				'singular_name' => _x('event', 'post type singular name'),
				'add_new' => _x('Dodaj nową aktualność', 'add new news'),
				'add_new_item' => __('Dodaj nową aktualność'),
				'edit_item' => __('Edycja'),
				'new_item' => __('Nowa aktualność'),
				'all_items' => __('Wszystkie aktualności'),
				'view_item' => __('Podgląd aktualności'),
				'search_items' => __('Szukaj aktualności'),
				'not_found' => __('Nie znaleziono aktualności'),
				'not_found_in_trash' => __('Nie znaleziono aktualności w koszu'),
				'parent_item_colon' => '',
				'menu_name' => 'Aktualności'
		),
		'description' => 'Aktualności firmy',
		'public' => true,
		'menu_position' => 5,
		'supports' => array('title', 'editor'),
		'has_archive' => false,
		'hierarchical' => true,
		'show_in_nav_menu' => true,
		'rewrite' => array(
			'slug' => 'news', //jezeli jest has_archive=false to slug moze byc taki jak podstrona np news/jakis-news
			'with_front' => true,
		)
	));

	//flush_rewrite_rules();
}


//-------------------------------------------------------------
// link correct - jezeli dany custom post ma byc w hierarchi
// np /news i pojedynczy element: /news/single-news ale nie ma with_front
//-------------------------------------------------------------
add_filter('post_link', 'changePostLink', 1, 3);
add_filter('post_type_link', 'changePostLink', 1, 3);
function changePostLink($permalink, $post_id, $leavename)
{
    if (strpos($permalink, 'singleNewsItem') === FALSE) return $permalink;

    $post = get_post($post_id);
    if (!$post) return $permalink;

    $permalink = str_replace('singleNewsItem', 'our-news', $permalink);
    return $permalink;
}