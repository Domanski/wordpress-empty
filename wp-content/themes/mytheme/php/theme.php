<?php
//-----------------------------------------------------------
// Theme Support
//-----------------------------------------------------------
if (!isset($content_width)) {
	$content_width = 900;
}


//-----------------------------------------------------------
// MENUS & NAVIGATION
//-----------------------------------------------------------
//register menu position in theme
add_action('init', 'register_menu');
function register_menu()
{
	register_nav_menus(array( // Using array to specify more menus if needed
			'header-menu' => 'Menu główne', // Main Navigation
			'footer-menu' => 'Menu w stopce', // Sidebar Navigation
	));
}


//-----------------------------------------------------------
// add active class in menu and remove other class
//-----------------------------------------------------------
add_filter( 'nav_menu_css_class', 'required_active_nav_class', 10, 2 );
function required_active_nav_class( $classes, $item ) {
	if ($item->current == 1 || $item->current_item_ancestor == true) {
		$classes = ["is-active"];
	} else {
		$classes = [];
	}
	return $classes;
}

add_filter( 'nav_menu_id', 'nav_menu_remove_id', 10, 2 );
function nav_menu_remove_id() {
	return false;
}

add_filter('nav_menu_item_id', 'required_active_nav_id', 10, 2);
function required_active_nav_id($classes, $item)
{
	return [];
}


//-----------------------------------------------------------
//sidebar
//-----------------------------------------------------------
add_action( 'widgets_init', 'joints_register_sidebars' );
function joints_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' =>'Sidebar',
		'description' => 'Boczna belka na stronach wpisów',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div></div>',
		'before_title' => '<h3 class="widgettitle">',
		'after_title' => '</h3><div class="widget-cnt">',
	));
}


//-----------------------------------------------------------
// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
//-----------------------------------------------------------
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
function add_slug_to_body_class($classes)
{
	global $post;
	if (is_home()) {
		$key = array_search('blog', $classes);
		if ($key > -1) {
			unset($classes[$key]);
		}
	} elseif (is_page()) {
		$classes[] = sanitize_html_class($post->post_name);
	} elseif (is_singular()) {
		$classes[] = sanitize_html_class($post->post_name);
	}

	return $classes;
}


//-----------------------------------------------------------
// Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
//-----------------------------------------------------------
add_action('init', 'noma_wp_pagination'); // Add our noma_ Pagination
function noma_wp_pagination()
{
	global $wp_query;
	$big = 999999999;
	echo paginate_links(array(
			'base' => str_replace($big, '%#%', get_pagenum_link($big)),
			'format' => '?paged=%#%',
			'current' => max(1, get_query_var('paged')),
			'total' => $wp_query->max_num_pages
	));
}


//-----------------------------------------------------------
// Custom View Article link to Post
//-----------------------------------------------------------
add_filter('excerpt_more', 'noma_view_article'); // Add 'View Article' button instead of [...] for Excerpts
function noma_view_article($more)
{
	global $post;
	return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'noma') . '</a>';
}


//-----------------------------------------------------------
// Numeric Page Navi (built into the theme by default)
//-----------------------------------------------------------
function noma_page_navi($before = '', $after = '')
{
	global $wpdb, $wp_query;
	$request = $wp_query->request;
	$posts_per_page = intval(get_query_var('posts_per_page'));
	$paged = intval(get_query_var('paged'));
	$numposts = $wp_query->found_posts;
	$max_page = $wp_query->max_num_pages;
	if ($numposts <= $posts_per_page) {
		return;
	}
	if (empty($paged) || $paged == 0) {
		$paged = 1;
	}
	$pages_to_show = 7;
	$pages_to_show_minus_1 = $pages_to_show - 1;
	$half_page_start = floor($pages_to_show_minus_1 / 2);
	$half_page_end = ceil($pages_to_show_minus_1 / 2);
	$start_page = $paged - $half_page_start;
	if ($start_page <= 0) {
		$start_page = 1;
	}
	$end_page = $paged + $half_page_end;
	if (($end_page - $start_page) != $pages_to_show_minus_1) {
		$end_page = $start_page + $pages_to_show_minus_1;
	}
	if ($end_page > $max_page) {
		$start_page = $max_page - $pages_to_show_minus_1;
		$end_page = $max_page;
	}
	if ($start_page <= 0) {
		$start_page = 1;
	}
	echo $before . '<nav class="page-navigation"><ul class="pagination">' . "";
	if ($start_page >= 2 && $pages_to_show < $max_page) {
		$first_page_text = __("First", 'jointstheme');
		echo '<li><a href="' . get_pagenum_link() . '" title="' . $first_page_text . '">' . $first_page_text . '</a></li>';
	}
	echo '<li>';
	previous_posts_link('<<');
	echo '</li>';
	for ($i = $start_page; $i <= $end_page; $i++) {
		if ($i == $paged) {
			echo '<li class="current"><a href="' . get_pagenum_link($i) . '">' . $i . '</a></li>';
		} else {
			echo '<li><a href="' . get_pagenum_link($i) . '">' . $i . '</a></li>';
		}
	}
	echo '<li>';
	next_posts_link('>>');
	echo '</li>';
	if ($end_page < $max_page) {
		$last_page_text = __("Last", 'jointstheme');
		echo '<li><a href="' . get_pagenum_link($max_page) . '" title="' . $last_page_text . '">' . $last_page_text . '</a></li>';
	}
	echo '</ul></nav>' . $after . "";
}


//-----------------------------------------------------------
// Custom Gravatar in Settings > Discussion
//-----------------------------------------------------------
add_filter('avatar_defaults', 'nomagravatar'); // Custom Gravatar in Settings > Discussion
function nomagravatar($avatar_defaults)
{
	$myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
	$avatar_defaults[$myavatar] = "Custom Gravatar";
	return $avatar_defaults;
}


