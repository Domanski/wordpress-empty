<?php
//-----------------------------------------------------------
// Google map api key
//-----------------------------------------------------------
add_action('acf/init', 'my_acf_init');
function my_acf_init() {
    acf_update_setting('google_api_key', 'AIzaSyBaaaMumR0ZoSO_l7lCgSxFqh5TTA3fS3Y');
}


//-----------------------------------------------------------
// Save fields groups to file
//-----------------------------------------------------------
add_filter('acf/settings/save_json', 'my_acf_json_save_point');
function my_acf_json_save_point( $path ) {
	$path = get_stylesheet_directory() . '/acf-fields/'; // this folder needs to be created first
	return $path;
}


//-----------------------------------------------------------
// Load field groups from file
//-----------------------------------------------------------
add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point( $paths ) {
	unset($paths[0]);
	$paths[] = get_stylesheet_directory() . '/acf-fields/'; // this folder needs to be created first
	return $paths;
}


//-----------------------------------------------------------
//custom toolbar in wysiwyg
//-----------------------------------------------------------
add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_wysiwyg_toolbars'  );
function my_wysiwyg_toolbars( $toolbars )
{
	// Add a new toolbar called "Very Simple"
	// - this toolbar has only 1 row of buttons
	//http://www.tinymce.com/wiki.php/TinyMCE3x:Buttons/controls
	$toolbars['Super simple' ] = array();
	$toolbars['Super simple' ][1] = array('bold' , 'italic' , 'alignleft', 'aligncenter', 'alignright', 'alignjustify', 'link' , 'hr');
	// Edit the "Full" toolbar and remove 'code'
	// - delet from array code from http://stackoverflow.com/questions/7225070/php-array-delete-by-value-not-key
	if( ($key = array_search('code' , $toolbars['Full' ][2])) !== false )
	{
		//unset( $toolbars['Full' ][2][$key] );
	}

	// remove the 'Basic' toolbar completely
	//unset( $toolbars['Basic' ] );

	// return $toolbars - IMPORTANT!
	return $toolbars;
}


//-----------------------------------------------------------
// uncomment this to hide the menu in wp-admin
//-----------------------------------------------------------
if (!is_admin()) {
	add_filter('acf/settings/show_admin', '__return_false');
}


//-----------------------------------------------------------
// add ACF options page and subpages
//-----------------------------------------------------------
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
			'page_title'    => 'Ustawienia strony',
			'menu_title'    => 'Ustawienia strony',
			'menu_slug'     => 'theme-general-settings',
			'capability'    => 'edit_posts',
			'redirect'      => false
	));

	acf_add_options_sub_page(array(
			'page_title'    => 'Ustawienia dodatkowe',
			'menu_title'    => 'Ustawienia dodatkowe',
			'parent_slug'   => 'theme-general-settings',
	));
}