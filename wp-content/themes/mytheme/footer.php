    <footer class="p-footer">

        <nav class="p-footer-menu" role="navigation">
            <?php insert_menu_ul('footer', 'p-footer-menu-list'); ?>
        </nav>

    </footer><!-- e: footer -->

    <?php wp_footer(); ?>
</div><!-- e: p-all -->
</body>
</html>
