<?php get_header(); ?>

<main class="page-main clearfix" role="main">
	<?php
	get_template_part('partials/breadcrumbs');
	?>
	<div class="page-center">
		<h2 class="page-title page-center"><?php the_title(); ?></h2>

		<article class="error-404 page-center">
			<h2 class="visuallyhidden">Błąd 404</h2>
			<div class="description">
				<strong>Przepraszamy</strong>
                <span>Podana strona nie istnieje.</span>
			</div>
			<a href="<?php echo home_url(); ?>" class="button">Idź do strony głównej</a>
		</article><!-- e: 404 -->

	</div><!-- e: page center -->
</main>

<?php get_footer(); ?>
