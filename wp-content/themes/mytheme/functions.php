<?php
include('php/theme.php');
include('php/custom-post.php');
include('php/admin.php');
include('php/acf.php');
include('php/clean-wp.php');
include('php/theme-functions.php');


//-----------------------------------------------------------
// Load scripts (header.php)
//-----------------------------------------------------------
add_action('init', 'load_scripts'); // Add Custom Scripts to wp_head
function load_scripts()
{
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }

	if (!is_admin()) {
		wp_deregister_script('jquery');
		wp_register_script('jquery', get_template_directory_uri() . '/js/vendor/jquery.min.js', array(), '1.11.4', false);
		wp_enqueue_script('jquery');

		wp_register_script('scripts', get_template_directory_uri() . '/js/scripts.js', array(), '1.0.0', true);
		wp_enqueue_script('scripts');

        //dodatkowe zmienne dla skryptów np. let url = ThemeParams.themeUrl
        //wp_localize_script('scripts', 'ThemeParams', array(
			//'themeUrl' => get_template_directory_uri()
		//));

	}
}





//-----------------------------------------------------------
// Load styles
//-----------------------------------------------------------
add_action('wp_enqueue_scripts', 'load_styles');
function load_styles()
{
	// wp_register_style('googleFonts', 'http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,300&subset=latin,latin-ext', array(), '1.0', 'all');
	// wp_enqueue_style('googleFonts');

	wp_register_style('styles', get_template_directory_uri() . '/css/style.css', array(), '1.0', 'all');
	wp_enqueue_style('styles');
}


//-----------------------------------------------------------
//disable search action
//-----------------------------------------------------------
//add_filter('get_search_form', create_function('$a', "return null;")); //disable search
//add_action('parse_query', 'fb_filter_query'); //disable search
//function fb_filter_query($query, $error = true)
// {

// 	if (is_search() && !is_admin()) {
// 		$query->is_search = false;
// 		$query->query_vars[s] = false;
// 		$query->query[s] = false;

// 		// to error
// 		if ($error == true)
// 			$query->is_404 = true;
// 	}
// }

