const path = require("path");
const webpack = require('webpack');

const url = "./wp-content/themes/mytheme";

module.exports = {
    entry: {
        main: [
            'webpack/hot/dev-server',
            'webpack-hot-middleware/client',
            url + '/src/js/app.js',
        ]
    },
    output: {
        filename: "script.min.js",
        path: path.resolve(__dirname, url + "/js"),
        publicPath: '/js/'
    },
    watch: false,
    mode: 'production',
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
};