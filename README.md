XXXXXXX
=======

Informacje o projekcie
----------------------

- Klient: XXXXXXX
- Projekt: XXXXXXX
- Wersja: 0.1.0
- Osoba techniczna: Andrzej Karwacki [andrzej.karwacki@nomasolutions.pl]
- Account: Adrian Gnatek [adrian.gnatek@spinhouse.pl]

Środowiska projektowe
---------------------

### Developerskie
- Adres: XXXXXXX-dev.nomasolutions.pl
- Rodzaj: standardowe środowisko Noma Solutions
- Status: Nieskonfigurowane

### Testowe
- Adres: XXXXXXX-test.nomasolutions.pl
- Rodzaj: standardowe środowisko Noma Solutions
- Status: Nieskonfigurowane

### Produkcyjne
- Adres: - XXXXXXX.pl
- Rodzaj: Zewnętrzny
- Status: Nieskonfigurowane

Konfiguracja
------------
1.	Przegrywamy wszystkie pliki na ftp (pomijając katalog /data)
2.	Importujemy baze z katalogu data/sql/XXXXXXX.sql
3.	Edytujemy plik /wp-config.php, ustawiawiamy dostepy do powyższej bazy.
4.	Po zaimportowaniu zmieniamy w bazie danych w tabeli wp_options dwa rekordy: 
        siteurl i home na adres na którym będzie stała strona http://XXXXXXX-dev.nomasolutions.pl
        Domyślnie tabela ta będzie miała ustawiony adres http://XXXXXXX-dev.nomasolutions.pl
5.	Ustawiamy przywileje dla plików i katalogów:
    5.1. Wszystkie katalogi powinny być ustawione na 755 lub 750
    5.2. Wszystkie pliki powinny być ustawione na 644 lub 640. Wyjątkiem jest wp-config.php który powinien być ustawiony na 440 lub 400
    5.3. Żaden katalog nie powinien być ustawiony na 777