const gulp                  = require("gulp");
const sass                  = require("gulp-sass");
const sourcemaps            = require("gulp-sourcemaps");
const autoprefixer          = require("gulp-autoprefixer");
const colors                = require("ansi-colors");
const notifier              = require("node-notifier");
const rename                = require("gulp-rename");
const wait                  = require("gulp-wait");
const csso                  = require("gulp-csso");
const browserSync           = require("browser-sync").create();
const gulpif                = require("gulp-if");

const webpack               = require("webpack");
const webpackDevMiddleware  = require("webpack-dev-middleware");
const webpackHotMiddleware  = require("webpack-hot-middleware");
const webpackConfig         = require("./webpack.config-dev.js");
const bundler               = webpack(webpackConfig);

sass.compiler = require('sass');

let productionMODE = true;
const url = "./wp-content/themes/mytheme"

const showError = function(err) {
    //console.log(err.toString());

    notifier.notify({
        title: "Error in sass",
        message: err.messageFormatted
    });

    console.log(colors.red("==============================="));
    console.log(colors.red(err.messageFormatted));
    console.log(colors.red("==============================="));
};

const server = function(cb) {
    const browserSyncConfig = {
        proxy: "przychodnia-feniks.test",
        host: "192.168.0.24", //"127.0.0.1",
        baseDir: ".",
        open:true,
        notify:false,
        online: true
    };

    if (!productionMODE) {
        browserSyncConfig.middleware = [
            webpackDevMiddleware(bundler, {
                publicPath: webpackConfig.output.publicPath,
                // stats: { colors: true }
            }),
            webpackHotMiddleware(bundler)
        ];
    }

    browserSync.init(browserSyncConfig);

    cb();
};

const css = function() {
    return gulp.src(url + "/src/scss/style.scss")
        .pipe(sourcemaps.init())
        .pipe(
            sass({
                outputStyle : productionMODE ? "compressed" : "expanded"
            }).on("error", showError)
        )
        .pipe(gulpif(
            productionMODE,
            autoprefixer()
        ))
        .pipe(rename({
            suffix: ".min",
            basename: "style"
        }))
        .pipe(gulpif(
            productionMODE,
            csso({
                restructure : false
            })
        ))
        .pipe(sourcemaps.write("."))
        .pipe(gulp.dest(url + "/css"))
        .pipe(browserSync.stream());
};

const js = function(cb) {
    return webpack(require("./webpack.config.js"), function(err, stats) {
        if (err) {
            throw err;
        }
        browserSync.reload();
        cb();
    });
};

const watch = function() {
    gulp.watch(url + "/src/scss/**/*.scss", {usePolling : true, delay: 300}, gulp.series(css));
    gulp.watch(url + "/**/*.php", {usePolling : true, delay: 300}).on("change", browserSync.reload);
    gulp.watch(url + "/**/*.[jpg|png|jpeg|gif]", {usePolling: true, delay: 300}).on("change", browserSync.reload);
    if (productionMODE) {
        gulp.watch(url + "/src/js/**/*.js", {usePolling: true, delay: 300}, gulp.series(js));
    }
};

const devModeTurnOn = function(cb) {
    productionMODE = false;
    cb();
};

exports.default = gulp.series(server, watch);
exports.develop = gulp.series(devModeTurnOn, server, watch);
exports.css = css;
exports.js = js;
